from sklearn.datasets import load_iris
import numpy as np
from sklearn import tree

iris = load_iris()
# printing various stuff
#
# print(iris.feature_names)
# print(iris.target_names)
# print(iris.data[0])
# for i in range(len(iris.data)):
#     print(iris.data)
#     print(iris.target)

test_idx = [0, 50, 100]

# training data
train_target = np.delete(iris.target, test_idx)
train_data = np.delete(iris.data, test_idx, axis=0)

# test data
test_data = iris.data[test_idx]
test_target = iris.target[test_idx]

# tree classifier
clf = tree.DecisionTreeClassifier()
clf.fit(train_data, train_target)

print(test_target)
print(clf.predict(test_data))
