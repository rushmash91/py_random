class food(object):
    def __init__(self, n, v, w):
        self.name = n
        self.value = v
        self.calorie = w

    def getvalue(self):
        return self.value

    def getcost(self):
        return self.calorie

    def getdensity(self):
        return self.getvalue() / self.getcost()

    def __str__(self):
        return self.name + ': <' + str(self.getvalue()) + ',' + str(self.getcost()) + '>'


def menu(names, value, calories):
    """makes the menu"""
    menu = []
    for i in len(value):
        menu.append(names[i], value[i], calories[i])

    return menu


def greedy(items, maxcost, keyfunction):

    itemscopy=sorted(items,key=keyfunction,reverse=True)
    result=[]
    (totalvalue,totalcost)=(0.0,0.0)
    for i in range(len (itemscopy)):
        if (totalcost+itemscopy[i].getcost()<=maxcost):
            result.append(itemscopy[i])
            totalcost+=itemscopy[i].getcost()
            totalvalue+=itemscopy[i].getvalue()

    return (result,totalvalue)


def testgreedy(items, constraints, keyfunction):

    taken, val = greedy(items,constraints,keyfunction)
    print('total value of items taken', val)
    for item in taken:
        print('  ', item)