def f(n, d):
    if n in d:
        return d[n]
    else:
        ans = n*f(n-1, d)
        d[n]=ans
        return ans


n = int(input('enter a number'))
d = {1: 1, 2: 2}

print(f(n, d))
